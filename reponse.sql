use asterix_db;
/*1 Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)*/
select * from `potion`;
/*2 Liste des noms des trophées rapportant 3 points. (2 lignes)*/
select NomCateg from categorie where nbpoints = 3; 
/*3 Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)*/
select NomVillage from village where NbHuttes > 35;
/*4 Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)*/
select NumTrophee from trophee where  DatePrise >cast('2052-05-01 00:00:00'as datetime) and DatePrise <cast('2052-06-30 00:00:00'as datetime);
/*5 Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)*/
select Nom from habitant where Nom like 'a%' and Nom like '%r%';
/*6 Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)*/
select DISTINCT NumHab from absorber where NumPotion = 1 or NumPotion = 2 or NumPotion = 4;
/*
select NumHab from absorber inner join potion on absorber.NumPotion = potion.NumPotion where lib*/
/*
/*7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10
lignes)*/

select Nom,NumTrophee,DatePrise,NomCateg from habitant inner join trophee on habitant.NumHab = trophee.NumPreneur inner join categorie on trophee.CodeCat = categorie.CodeCat;
/*8. Nom des habitants qui habitent à Aquilona. (7 lignes)*/
select  Nom from habitant inner join village on habitant.NumVillage = village.NumVillage where village.NomVillage = 'Aquilona';
/* 9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)*/
select nom from habitant inner join trophee on trophee.NumPreneur = habitant.NumHab where CodeCat like "BLT";
/* 10 Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant
principal. (3 lignes)*/
select LibPotion, Formule, ConstituantPrincipal from potion inner join fabriquer on potion.NumPotion = fabriquer.NumPotion where NumHab = 4;
/*Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)*/
select LibPotion from potion inner join absorber on potion.NumPotion = absorber.NumPotion  where NumHab = 3;
/*select LibPotion from potion inner join absorber on potion.NumPotion = absorber.NumPotion inner join habitant on habitant.NumHab = absorber.potion where NumHab = '3';*/



/*12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro
3. (4 lignes)*/
select nom from habitant
inner join absorber
on habitant.numhab = absorber.numhab
inner join fabriquer
on 
where absorber
group by nom
;

select nom from habitant inner join absorber on habitant.NumHab = absorber.NumHab inner join fabriquer on absorber.NumPotion = fabriquer.NumPotion where fabriquer.NumPotion = 3;

/* 13 Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)*/
select nom from habitant inner join absorber on habitant.NumHab = absorber.NumHab inner join fabriquer on absorber.NumPotion = fabriquer.NumPotion where fabriquer.NumPotion = 2 or fabriquer.NumPotion = 5;

/* 14. Nom des habitants dont la qualité n'est pas renseignée. (3 lignes)*/ 
select nom from habitant  where NumQualite is NULL ;

/* 15 Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52. (3 lignes) */
select DISTINCT nom from habitant inner join absorber on absorber.NumPotion where absorber.NumPotion = 1 or absorber.DateA >cast('2052-02-00 00:00:00'as datetime) and DateA <cast('2052-02-29 00:00:00'as datetime);

/* 16. Nom et âge des habitants par ordre alphabétique. (22 lignes)*/
select nom, age from habitant ORDER by Nom ;
/*17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom
du village. (3 lignes)*/
select NumResserre from resserre inner join village on village.NumVillage = resserre.NumVillage ORDER by Superficie desc limit 4;
/*18. Nombre d'habitants du village numéro 5. (4)*/
select NumHab from habitant where NumVillage = 5;
/*19. Nombre de points gagnés par Goudurix. (5)*/
select nbpoints from categorie inner join habitant on 
select sum(NbPoints) from trophee inner join categorie on trophee.CodeCat = categorie.CodeCat where NumPreneur = 16;

/* 20 */
 SELECT MIN(DatePrise) FROM trophee ;

/* 21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)*/

SELECT sum(Quantite) FROM absorber INNER JOIN potion ON absorber.NumPotion = potion.NumPotion WHERE potion.LibPotion = "potion magique n°2";
/* 22 Superficie la plus grande. (895)*/
select max(Superficie) from resserre;
/* 23. Nombre d'habitants par village (nom du village, nombre). (7 lignes)*/
select NomVillage, NumHab from habitant  inner join village on village.NumVillage = habitant.numhab;
/* 24. Nombre de trophées par habitant (6 lignes)*/
/*  pas bon */
select nom,count(*) from trophee inner join habitant on habitant.numhab = trophee.NumPreneur GROUP BY habitant.nom;

/* 25. Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes) */
select NomProvince,AVG(age) from habitant inner join village on habitant.NumVillage = village.NumVillage inner join province on village.NumProvince = province.NumProvince group by NomProvince;
/*26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9
lignes)*/
select nom, count(*) from potion inner join absorber on potion.NumPotion = absorber.NumPotion inner join habitant on habitant.numhab = absorber.numhab GROUP BY habitant.nom;
/*27. Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)*/
select nom from habitant inner join absorber on absorber.numhab = habitant.numhab inner join potion on potion.NumPotion = absorber.NumPotion where LibPotion like'Potion Zen' and Quantite > 2;
/*28. Noms des villages dans lesquels on trouve une resserre (3 lignes)*/
select NomVillage from village inner join resserre on village.NumVillage = resserre.NumVillage;
/*29. Nom du village contenant le plus grand nombre de huttes. (Gergovie)*/
select NomVillage from village where NbHuttes = (select max(NbHuttes) from village)